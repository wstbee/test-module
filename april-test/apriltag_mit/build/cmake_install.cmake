# Install script for directory: /home/naverlabs/workspace/test-module/april-test/apriltag_mit

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr/local")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY FILES "/home/naverlabs/workspace/test-module/april-test/apriltag_mit/build/lib/libapriltags.a")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/AprilTags" TYPE FILE FILES
    "/home/naverlabs/workspace/test-module/april-test/apriltag_mit/AprilTags/Tag36h9.h"
    "/home/naverlabs/workspace/test-module/april-test/apriltag_mit/AprilTags/Tag16h5.h"
    "/home/naverlabs/workspace/test-module/april-test/apriltag_mit/AprilTags/Gaussian.h"
    "/home/naverlabs/workspace/test-module/april-test/apriltag_mit/AprilTags/UnionFindSimple.h"
    "/home/naverlabs/workspace/test-module/april-test/apriltag_mit/AprilTags/GLine2D.h"
    "/home/naverlabs/workspace/test-module/april-test/apriltag_mit/AprilTags/TagFamily.h"
    "/home/naverlabs/workspace/test-module/april-test/apriltag_mit/AprilTags/Tag36h11_other.h"
    "/home/naverlabs/workspace/test-module/april-test/apriltag_mit/AprilTags/Tag36h11.h"
    "/home/naverlabs/workspace/test-module/april-test/apriltag_mit/AprilTags/GLineSegment2D.h"
    "/home/naverlabs/workspace/test-module/april-test/apriltag_mit/AprilTags/FloatImage.h"
    "/home/naverlabs/workspace/test-module/april-test/apriltag_mit/AprilTags/Quad.h"
    "/home/naverlabs/workspace/test-module/april-test/apriltag_mit/AprilTags/Edge.h"
    "/home/naverlabs/workspace/test-module/april-test/apriltag_mit/AprilTags/pch.h"
    "/home/naverlabs/workspace/test-module/april-test/apriltag_mit/AprilTags/Tag25h9.h"
    "/home/naverlabs/workspace/test-module/april-test/apriltag_mit/AprilTags/GrayModel.h"
    "/home/naverlabs/workspace/test-module/april-test/apriltag_mit/AprilTags/TagDetector.h"
    "/home/naverlabs/workspace/test-module/april-test/apriltag_mit/AprilTags/Gridder.h"
    "/home/naverlabs/workspace/test-module/april-test/apriltag_mit/AprilTags/XYWeight.h"
    "/home/naverlabs/workspace/test-module/april-test/apriltag_mit/AprilTags/Homography33.h"
    "/home/naverlabs/workspace/test-module/april-test/apriltag_mit/AprilTags/TagDetection.h"
    "/home/naverlabs/workspace/test-module/april-test/apriltag_mit/AprilTags/Tag16h5_other.h"
    "/home/naverlabs/workspace/test-module/april-test/apriltag_mit/AprilTags/MathUtil.h"
    "/home/naverlabs/workspace/test-module/april-test/apriltag_mit/AprilTags/Segment.h"
    "/home/naverlabs/workspace/test-module/april-test/apriltag_mit/AprilTags/Tag25h7.h"
    )
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/pkgconfig" TYPE FILE FILES "/home/naverlabs/workspace/test-module/april-test/apriltag_mit/build/lib/pkgconfig/apriltags.pc")
endif()

if(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  include("/home/naverlabs/workspace/test-module/april-test/apriltag_mit/build/example/cmake_install.cmake")

endif()

if(CMAKE_INSTALL_COMPONENT)
  set(CMAKE_INSTALL_MANIFEST "install_manifest_${CMAKE_INSTALL_COMPONENT}.txt")
else()
  set(CMAKE_INSTALL_MANIFEST "install_manifest.txt")
endif()

string(REPLACE ";" "\n" CMAKE_INSTALL_MANIFEST_CONTENT
       "${CMAKE_INSTALL_MANIFEST_FILES}")
file(WRITE "/home/naverlabs/workspace/test-module/april-test/apriltag_mit/build/${CMAKE_INSTALL_MANIFEST}"
     "${CMAKE_INSTALL_MANIFEST_CONTENT}")
