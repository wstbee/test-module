# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/naverlabs/workspace/test-module/april-test/apriltag_mit/src/Edge.cc" "/home/naverlabs/workspace/test-module/april-test/apriltag_mit/build/CMakeFiles/apriltags.dir/src/Edge.cc.o"
  "/home/naverlabs/workspace/test-module/april-test/apriltag_mit/src/FloatImage.cc" "/home/naverlabs/workspace/test-module/april-test/apriltag_mit/build/CMakeFiles/apriltags.dir/src/FloatImage.cc.o"
  "/home/naverlabs/workspace/test-module/april-test/apriltag_mit/src/GLine2D.cc" "/home/naverlabs/workspace/test-module/april-test/apriltag_mit/build/CMakeFiles/apriltags.dir/src/GLine2D.cc.o"
  "/home/naverlabs/workspace/test-module/april-test/apriltag_mit/src/GLineSegment2D.cc" "/home/naverlabs/workspace/test-module/april-test/apriltag_mit/build/CMakeFiles/apriltags.dir/src/GLineSegment2D.cc.o"
  "/home/naverlabs/workspace/test-module/april-test/apriltag_mit/src/Gaussian.cc" "/home/naverlabs/workspace/test-module/april-test/apriltag_mit/build/CMakeFiles/apriltags.dir/src/Gaussian.cc.o"
  "/home/naverlabs/workspace/test-module/april-test/apriltag_mit/src/GrayModel.cc" "/home/naverlabs/workspace/test-module/april-test/apriltag_mit/build/CMakeFiles/apriltags.dir/src/GrayModel.cc.o"
  "/home/naverlabs/workspace/test-module/april-test/apriltag_mit/src/Homography33.cc" "/home/naverlabs/workspace/test-module/april-test/apriltag_mit/build/CMakeFiles/apriltags.dir/src/Homography33.cc.o"
  "/home/naverlabs/workspace/test-module/april-test/apriltag_mit/src/MathUtil.cc" "/home/naverlabs/workspace/test-module/april-test/apriltag_mit/build/CMakeFiles/apriltags.dir/src/MathUtil.cc.o"
  "/home/naverlabs/workspace/test-module/april-test/apriltag_mit/src/Quad.cc" "/home/naverlabs/workspace/test-module/april-test/apriltag_mit/build/CMakeFiles/apriltags.dir/src/Quad.cc.o"
  "/home/naverlabs/workspace/test-module/april-test/apriltag_mit/src/Segment.cc" "/home/naverlabs/workspace/test-module/april-test/apriltag_mit/build/CMakeFiles/apriltags.dir/src/Segment.cc.o"
  "/home/naverlabs/workspace/test-module/april-test/apriltag_mit/src/TagDetection.cc" "/home/naverlabs/workspace/test-module/april-test/apriltag_mit/build/CMakeFiles/apriltags.dir/src/TagDetection.cc.o"
  "/home/naverlabs/workspace/test-module/april-test/apriltag_mit/src/TagDetector.cc" "/home/naverlabs/workspace/test-module/april-test/apriltag_mit/build/CMakeFiles/apriltags.dir/src/TagDetector.cc.o"
  "/home/naverlabs/workspace/test-module/april-test/apriltag_mit/src/TagFamily.cc" "/home/naverlabs/workspace/test-module/april-test/apriltag_mit/build/CMakeFiles/apriltags.dir/src/TagFamily.cc.o"
  "/home/naverlabs/workspace/test-module/april-test/apriltag_mit/src/UnionFindSimple.cc" "/home/naverlabs/workspace/test-module/april-test/apriltag_mit/build/CMakeFiles/apriltags.dir/src/UnionFindSimple.cc.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "include"
  "/usr/local/include"
  "../AprilTags"
  "../."
  "/opt/local/include"
  "/usr/local/include/opencv"
  "/usr/local/include/eigen3"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
