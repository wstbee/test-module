import cv2
import numpy as np

for i in range(586):
    num = ""
    if i < 10:
        num = "00"+str(i)
    elif i < 100:
        num = "0"+str(i)
    else:
        num = str(i)

    img = cv2.imread("./tag36h11/tag36_11_00"+num+".png",2)
    res = np.zeros((1000,1000,1), dtype = "uint8")
    for j in range(10):
        for k in range(10):
            res[j*100:(j+1)*100,k*100:(k+1)*100] = img[j,k]

    cv2.imwrite("./tag36h11_resized/tag"+str(i)+".png", res)
