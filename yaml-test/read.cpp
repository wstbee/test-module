#include "opencv2/core.hpp"
#include "iostream"

using namespace std;
using namespace cv;

int main(int argc, char** argv)
{
    if(argc < 1)
        cout << "usage: read test.yaml" << endl;

    FileStorage fs(argv[1], FileStorage::READ);

    int a = fs["test.a"];
    float b = fs["test.b"];
    int c = fs["test.c"];

    cout << a << endl << b << endl << c << endl;
    // cout << fs["test.a"] << endl;
    // cout << fs["test.b"] << endl;
    // cout << fs["test.c"] << endl;

    return 0;
}
