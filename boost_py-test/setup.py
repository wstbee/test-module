from distutils.core import setup, Extension

module_hello = Extension('hello',
                         include_dirs = ['usr/include'],
                         libraries = ['boost_python'],
                         library_dirs = ['usr/lib'],
                         sources = ['greet.cpp'])

setup(name = 'hello',
      version = '0.1',
      description = 'This is a test of Boost.Python',
      ext_modules = [module_hello])

