#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/ghryou/workspace/ros-test/catkin-test/devel:$CMAKE_PREFIX_PATH"
export PWD="/home/ghryou/workspace/ros-test/catkin-test/build"
export ROSLISP_PACKAGE_DIRECTORIES="/home/ghryou/workspace/ros-test/catkin-test/devel/share/common-lisp"
export ROS_PACKAGE_PATH="/home/ghryou/workspace/ros-test/catkin-test/src:$ROS_PACKAGE_PATH"